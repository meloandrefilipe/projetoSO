#ifndef MEDITDEFAULTS_H
#define MEDITDEFAULTS_H

typedef struct estruturaCliente{
	int pid;
	char username[9];
	int linha;
} cliente;

typedef struct estruturaThreads{
	pthread_t id;
	int nthread;
	int nusers;
	int *users;
	char **text;
	char **valid;
	int nlinhas;
	int ncolunas;
	cliente *listaClientes;
	int nclientes;
	char *database;
	char *pipename;
	int npipes;
} thread;

typedef struct estruturaLinha{
	int pid;
	int nlinha;
	char linha[100];
	int submit;
	int request;
} linhas;

typedef struct estruturaResposta{
	int pid;
	int tipo; 
	int nlinha;
	linhas msg;
	int pipe;
} resposta;


#endif
