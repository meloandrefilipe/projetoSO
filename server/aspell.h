//
// Created by andre on 02/01/19.
//

#ifndef PROJECT_ASPELL_H
#define PROJECT_ASPELL_H

int aspell(char *str);
int checkSpell(char *str);
char **checkSpell_v2(char **str, int lines, int cols);

#endif //PROJECT_ASPELL_H
