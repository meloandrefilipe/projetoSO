#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <ctype.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <stdbool.h> 
#include <sys/time.h>
#include <sys/stat.h>
#include <pthread.h>
#include <fcntl.h>
#include "server.h"
#include "medit.h"
#include "aspell.h"

#define dataSize 1024

char** readFile(char * file,int max_letters,int max_lines);
void settings();
void thread_exit();
void * enviar_resposta(void *p);
void tratar_resposta(resposta * nova, thread * poolpipes);
void * thread_clientePipe(void* p);
void * thread_serverPipe(void* p);
int checkUser(char * filename, char * username);
int checkFile(const char *filename);
int saveFile(char * filename, char **data , int size);
void estatisticas(thread * t);

int main(int argc, char **argv, char **envv){
	char database[dataSize];
	char *main_pipe = "serverPipe";
	int npipes = atoi(getenv("MEDIT_PIPES")) + 1;
	strcpy(database,"medit.db");
	if(argc == 3) {
		if (strcmp(argv[1], "-f") == 0) {
			strcpy(database, argv[2]);
		} else if (strcmp(argv[1], "-p") == 0) {
			main_pipe = argv[2];
		} else if (strcmp(argv[1], "-n") == 0) {
			npipes = atoi(argv[2]) +1;
		} else {
			printf("\nArgumento nao existe\n\n");
			return -1;
		}
	}
	if(getenv("MEDIT_MAXLINES")==NULL || getenv("MEDIT_MAXCOLUMNS")==NULL){
		printf("As variaveis de ambiente ainda nao foram criadas!\n");
		return-1;
	}
	int max_columns = atoi(getenv("MEDIT_MAXCOLUMNS"));
	int max_lines = atoi(getenv("MEDIT_MAXLINES"));
	char **texto = readFile("teste.txt",max_columns,max_lines);
	cliente *listaClientes = malloc(sizeof(cliente) * atoi(getenv("MEDIT_MAXUSERS")));

	char **valid = malloc(sizeof(char *) * max_lines);
	for (int i = 0; i < max_lines; ++i) {
		valid[i] = (char *)malloc(sizeof(char) * (max_columns+2));
	}
	for (int n = 0; n < max_lines ; ++n) {
		strcpy(valid[n],texto[n]);
	}
	
	char s[26];
	char b[25];
	char * file;
	int i,j,k,l,ret;
	for(j=0;j<40;j++){printf("\n");}
	thread * poolpipes = (thread *) malloc(npipes * sizeof(thread));

	poolpipes[0].nclientes = 0;
	poolpipes[0].listaClientes = listaClientes;
	poolpipes[0].ncolunas = max_columns;
	poolpipes[0].nlinhas = max_lines;
	poolpipes[0].text = texto;
	poolpipes[0].valid = valid;
	poolpipes[0].nthread = 0;
	poolpipes[0].nusers = 0;
	poolpipes[0].users = NULL;
	poolpipes[0].database = database;
	poolpipes[0].pipename = main_pipe;
	poolpipes[0].npipes = npipes;
	ret = pthread_create(&poolpipes[0].id,NULL,thread_serverPipe,(void *) poolpipes);
	if(ret != 0){
		printf("\n\nErro thread\n\n");
		return -1;
	}
//------------
	for(i=1; i < npipes; i++){
		poolpipes[i].nthread = i;
		poolpipes[i].nusers = 0;
		poolpipes[i].listaClientes = NULL;
		poolpipes[i].text = texto;
		poolpipes[i].ncolunas = max_columns;
		poolpipes[i].nlinhas = max_lines;
		poolpipes[i].database = database;
		poolpipes[i].users = (int *)  malloc(atoi(getenv("MEDIT_MAXUSERS")) * sizeof(int));
		ret = pthread_create(&poolpipes[i].id,NULL,thread_clientePipe,(void *) poolpipes);
		if(ret != 0){
			printf("\n\nErro thread\n\n");
			return -1;
		}
	}

//------------

	while(1){
		printf("------- Servidor MEDIT -------\n\nLista de comandos:\nsettings\nload\nsave\nfree\nstatistics\nusers\ntext\nexit\n\nMEDIT:");
		fflush(stdin);
		i= (int) strlen(s);
		s[i-1]='\0';
		fgets(s,26,stdin);
		for(j=0;j<30;j++){
		    printf("\n");
		}
		if(strncmp(s,"settings", 8)==0){
			settings();
		}
		if(strncmp(s,"exit", 4)==0){
			break;
		}
		if(strncmp(s,"load",4)==0){
			if(s[4] != ' ' || s[5] == '\0'|| s[5] == '\n'){printf("Sintaxe: load filename\n\n\n");}
			else{
                char *c;
                strtok (s," ");
                file = strtok (NULL, " ");
                if((c=strchr(file, '\n'))!=NULL){*c='\0';}
                resposta nova;
                if(checkFile(file)){
                    printf("O ficheiro nao existe!\n");
                }else {
                    char **newText = readFile(file, max_columns, max_lines);
			free(poolpipes[0].text);
			poolpipes[0].text = newText;
			for (int n = 0; n < max_lines ; ++n) {
				strcpy(poolpipes[0].valid[n],poolpipes[0].text[n]);
			}
                    for (i=0; i < max_lines ; i++) {
                        nova.msg.nlinha=i+1;
                        nova.tipo = 0;
			nova.pid = 0;

			strcpy(nova.msg.linha, poolpipes[0].text[i]);
                        tratar_resposta(&nova, poolpipes);
			sleep(0.1);
                    }
                    poolpipes[0].text = newText;
                }
			}
		}
		if(strncmp(s,"save", 4)==0){
			if(s[4] != ' ' || s[5] == '\0'|| s[5] == '\n'){printf("Sintaxe: save filename\n\n\n");}
			else{
			    	strtok (s," ");
			    	file = strtok (NULL, " ");
			    	saveFile(file,poolpipes[0].valid,max_lines);
			}
		}
		if(strncmp(s,"free", 4)==0){
			if(s[4] != ' ' || s[5] == '\0'|| s[5] == '\n'){printf("Sintaxe: free nline \n\n\n");}
			else{
				k=0;
				for(i=0;s[i] != ' ';i++);
				for(;s[i] == ' ';i++);
				for(j=0;s[i] != '\0' && s[i] != '\n' && s[i] != ' ';i++,j++){b[j]=s[i];};
				b[j]='\0';
				for(j=0;b[j] != '\0';j++){
					if(b[j] < '0' || b[j] > '9'){k = 1;};
				}
				if(k==0){
					printf("\n\nSintaxe correta.\nNumero de linha: %s\nComando não implementado\n\n", b);
				}else{
					printf("Sintaxe: free nline \n\n\n");
				}
			}
		}
		if(strncmp(s,"statistics\n", 11)==0){
			estatisticas(poolpipes);
		}
		if(strncmp(s,"users", 5)==0){
			if(s[5] == ' ' || s[5] == '\0'|| s[5] == '\n'){
				for(i=0; i<poolpipes[0].nclientes; i++){
					printf("\n---------------------\n\nCliente: %s\nPid: %d\nLinha a editar: %d\n", poolpipes[0].listaClientes[i].username, poolpipes[0].listaClientes[i].pid, poolpipes[0].listaClientes[i].linha);
					
				}
			}

		}
		if(strncmp(s,"text\n", 5)==0){
			int userAtLine=0;
			char *c;
			for(i=0;i<poolpipes[0].nlinhas;i++){
				for(j=0;j<poolpipes[0].nclientes && poolpipes[0].listaClientes[j].linha != i + 1;j++);
				if(j==poolpipes[0].nclientes){
					if(i>=9){printf("\t\tLinha:%d\t%s\n", i+1,poolpipes[0].text[i]);}
					else{printf("\t\tLinha:%d\t\t%s\n", i+1,poolpipes[0].text[i]);}
					
					
				}else{
					if((c=strchr(poolpipes[0].listaClientes[j].username, '\n'))!=NULL){
						*c='\0';
					}
					if(i>=9){printf("%s\t\tLinha:%d\t%s\n", poolpipes[0].listaClientes[j].username,i+1,poolpipes[0].text[i]);}
					else{printf("%s\t\tLinha:%d\t\t%s\n", poolpipes[0].listaClientes[j].username,i+1,poolpipes[0].text[i]);}
				}
			}
		}
	}
	for(i=0; i<npipes; i++){
		pthread_kill(poolpipes[i].id, SIGUSR1);

	}
	for(i=0; i<npipes; i++){
		pthread_join(poolpipes[i].id, NULL);
	}

	resposta exit;
	exit.tipo = -1;
	tratar_resposta(&exit,poolpipes);

	char temp[12]= "pipe", temp1[12], temp2[12];
	strcpy(temp2,temp);
	for(i=1; i<npipes; i++){
		sprintf(temp1,"%d",i);
		strcat(temp,temp1);
		remove(temp);
		strcpy(temp,temp2);
	}
	remove(poolpipes[0].pipename);
	for(i=1;i<npipes;i++){free(poolpipes[i].users);}
	free(poolpipes);
	free(valid);
	free(texto);
	free(listaClientes);

	
	
	
	return 1;
}



char** readFile(char * file,int max_letters,int max_lines){
	FILE * f;
	int i;
	char *c;
	if((f = fopen(file,"r")) == NULL){
		printf("Erro ao abrir o ficheiro");
		return NULL;
	}
	char **texto = malloc(sizeof(char *) * max_lines);
	for (i = 0; i < max_lines; ++i) {
    		texto[i] = (char *)malloc(sizeof(char) * (max_letters+2));
	}
	for(i=0; i < max_lines;i++){
		fgets(texto[i],max_letters+1,f);
		
		if((c=strchr(texto[i], '\n'))!=NULL){
			*c=' ';
		}
		for(int j=0; j < max_letters; j++){
			
			if(texto[i][j]== '\0'){
				texto[i][j]=' ';
			}
		}

		texto[i][max_letters]='\0';
	}



	fclose(f);
	return texto;
}

int saveFile(char * filename, char **data , int size){
	char *c;	
	if((c=strchr(filename, '\n'))!=NULL){
			*c='\0';
		}
	if (checkFile(filename)){
		FILE *file = fopen(filename,"w");
		if (file != NULL) {
			for (int i = 0; i < size; ++i) {
				fputs(data[i], file);
				fputs("\n",file);
			}
			fclose(file);
			printf("Informacao guardada com sucesso!\n");
			return 1;
		}else {
			printf("Erro ao criar o ficheiro %s", filename);
			return 0;
		}
	}else{
		int option = 0;
		printf("O ficheiro ja existe! Deseja sobrepor a nova informacao a velha?\n");
		printf(" 1- Sim \n 2- Sair \n");
		while(option < 1 || option > 2) {
			scanf("%d", &option);
		}
		if (option == 1){
			FILE *file = fopen(filename,"w");
			if (file != NULL) {
				for (int i = 0; i < size; ++i) {
					fputs(data[i], file);
					fputs("\n",file);
				}
				fclose(file);
				printf("Informacao guardada com sucesso!\n");
				return 1;
			}else {
				printf("Erro ao sobrepor a informacao no ficheiro %s", filename);
				return 0;
			}
		}
	}
}
int checkFile(const char *filename){
	FILE *fp = fopen (filename, "r");
	if (fp!=NULL) {
		fclose(fp);
		return 0;
	}else {
		return 1;
	}
}


void settings(){
	printf("\n\n\n\n\n\n\nMEDIT_TIMEOUT=%s\n", getenv("MEDIT_TIMEOUT"));
	printf("MEDIT_MAXUSERS=%s\n", getenv("MEDIT_MAXUSERS"));
	printf("MEDIT_MAXLINES=%s\n", getenv("MEDIT_MAXLINES"));
	printf("MEDIT_MAXCOLUMNS=%s\n\n", getenv("MEDIT_MAXCOLUMNS"));
	printf("MEDIT_PIPES=%s\n\n", getenv("MEDIT_PIPES"));
}

void thread_exit(){
	pthread_exit(NULL);
}

void * enviar_resposta(void *p){
	resposta * nova = (resposta *) p;
	char temp[50] = "../client/";
	char strPid[12];
	sprintf(strPid, "%d", nova->pid);
	strcat(temp,strPid);
	int f = open(temp, O_WRONLY);
	
	write(f, nova, sizeof(resposta));
	close(f);
	return NULL;
}


void tratar_resposta(resposta * nova, thread * poolpipes){
	int i,j,ret;
	pthread_t d;
	int npipes = poolpipes[0].npipes;
	int pid_cliente = nova->pid; // PID DE QUEM GEROU O GEROU O BROADCAST (para nao enviar a linha à pessoa que fez a alteração)
	switch(nova->tipo){
		case -1://broadcast fecho de servidor
			for(i=0; i < poolpipes[0].nclientes ; i++){
				nova->pid = poolpipes[0].listaClientes[i].pid;
				ret = pthread_create(&d,NULL,enviar_resposta,(void *) nova);
				pthread_join(d,NULL);
			}
			break;
	
		case 0:	//broadcast de linhas
			for(i=1; i < npipes; i++){
				for(j=0; j < poolpipes[i].nusers; j++){
					if(poolpipes[i].users[j] != pid_cliente){
						nova->pid = poolpipes[i].users[j];
						ret = pthread_create(&d,NULL,enviar_resposta,(void *) nova);
						if(ret != 0){
								printf("\n\nErro thread\n\n");
								return;
						}
						pthread_join(d,NULL);
					}
				}
			}
			break;

		case 1: // resposta especifica a 1 PID (ex: acesso a uma linha)
			ret = pthread_create(&d,NULL,enviar_resposta,(void *) nova);
			pthread_join(d,NULL);
			break;

		case 2: // resposta do nome do pipe atribuido pelo servidor e envio do texto atual no servidor
			ret = pthread_create(&d,NULL,enviar_resposta,(void *) nova);
			if(ret != 0){
					printf("\n\nErro thread\n\n");
					return;
			}
			pthread_join(d,NULL);
			for(i=0; i < poolpipes[0].nlinhas; i++){
				strcpy(nova->msg.linha,poolpipes[0].text[i]);
				ret = pthread_create(&d,NULL,enviar_resposta,(void *) nova);
				pthread_join(d,NULL);
				
			}
			break;
		case 3: // USERNAME NÃO EXISTE
			ret = pthread_create(&d,NULL,enviar_resposta,(void *) nova);
			pthread_join(d,NULL);
			break;

		case 4: // USERNAME já esta logado
			ret = pthread_create(&d,NULL,enviar_resposta,(void *) nova);
			pthread_join(d,NULL);
			break;
		case 5:	//Avisa que a LINHA FOI OCUPADA
			for(i=1; i < npipes; i++){
				for(j=0; j < poolpipes[i].nusers; j++){
					if(poolpipes[i].users[j] != pid_cliente){
						nova->pid = poolpipes[i].users[j];
						ret = pthread_create(&d,NULL,enviar_resposta,(void *) nova);
						if(ret != 0){
								printf("\n\nErro thread\n\n");
								return;
						}
						pthread_join(d,NULL);
					}
				}
			}
			break;
		case 6:	//Avisa que a LINHA deixou de estar OCUPADA
			for(i=1; i < npipes; i++){
				for(j=0; j < poolpipes[i].nusers; j++){
					if(poolpipes[i].users[j] != pid_cliente){
						nova->pid = poolpipes[i].users[j];
						ret = pthread_create(&d,NULL,enviar_resposta,(void *) nova);
						if(ret != 0){
								printf("\n\nErro thread\n\n");
								return;
						}
						pthread_join(d,NULL);
					}
				}
			}
			break;
		case 7: // SEM ERROS NA FRASE
			ret = pthread_create(&d,NULL,enviar_resposta,(void *) nova);
			pthread_join(d,NULL);
			break;
		case 8: // ERROS NA FRASE
			ret = pthread_create(&d,NULL,enviar_resposta,(void *) nova);
			pthread_join(d,NULL);
			break;
		
	}
}

void * thread_clientePipe(void* p){
	signal(SIGUSR1, thread_exit);
	thread * poolpipes = (thread *) p;
	char pipe[12] = "pipe", temp[12];
	char lastLine[100];
	int erros;
	int i,j;
	int check;
	for(i=0; i < poolpipes[0].npipes && pthread_self() != poolpipes[i].id ; i++);
	sprintf(temp,"%d",poolpipes[i].nthread);
	strcat(pipe,temp);	
	mkfifo(pipe, 0666);
	int f = open(pipe, O_RDONLY);
	int f1 = open(pipe, O_WRONLY);

	linhas nova;
	resposta r;


	while(1){
		read(f,&nova,sizeof(linhas));
		if(nova.request == 1){
			r.tipo = 1;
			r.pid = nova.pid;
			check = 0;
			for(j=0;j<poolpipes[0].nclientes;j++){
				if(poolpipes[0].listaClientes[j].linha == nova.nlinha){
					check = 1;
				}
			}
			if(check == 0){
				for(j=0;j<poolpipes[0].nclientes;j++){
					if(nova.pid == poolpipes[0].listaClientes[j].pid){
						poolpipes[0].listaClientes[j].linha = nova.nlinha;
					}
				}
			}
			r.nlinha = check;
			tratar_resposta(&r, poolpipes);
			if(check == 0){
				r.tipo = 5;
				r.nlinha = nova.nlinha;
				tratar_resposta(&r, poolpipes);
			}
			nova.nlinha = -99;
		}
		if(nova.nlinha > 0){
			r.tipo = 0;
			r.pid = nova.pid;
			strncpy(r.msg.linha, nova.linha, atoi(getenv("MEDIT_MAXCOLUMNS"))+2);
			strncpy(poolpipes->text[nova.nlinha - 1], nova.linha, atoi(getenv("MEDIT_MAXCOLUMNS"))+2);
			r.msg.nlinha = nova.nlinha;
			if (nova.submit == 1){
				// Aqui quer dizer que o utilizador carregou no enter ou seja temos de verificar e depois mandar uma resposta ao utilizador se houver erros
				


				erros = checkSpell(poolpipes->text[nova.nlinha - 1]); //devolve a quantidade de erros na frase;
				if (erros == 0){
					r.tipo = 7;
					strncpy(poolpipes->valid[nova.nlinha - 1], nova.linha, atoi(getenv("MEDIT_MAXCOLUMNS"))+2);
					tratar_resposta(&r, poolpipes);				
					r.tipo = 6;
					r.nlinha = nova.nlinha;
					tratar_resposta(&r, poolpipes);
					for(j=0;j<poolpipes[0].nclientes;j++){
						if(nova.pid == poolpipes[0].listaClientes[j].pid){
							poolpipes[0].listaClientes[j].linha = 0;
						}
					}
				}else{
					r.tipo = 8;
					tratar_resposta(&r, poolpipes);
				}
			}
			r.tipo = 0;
			tratar_resposta(&r, poolpipes);
			strncpy(lastLine, poolpipes->text[nova.nlinha - 1], atoi(getenv("MEDIT_MAXCOLUMNS"))+2);
		}
		if(nova.nlinha == -1){
			//ir até ao pid
			for(j=0; j < poolpipes[i].nusers && poolpipes[i].users[j] != nova.pid ; j++ );
			//puxar todos os seguintes para trás
			for(j++; j < poolpipes[i].nusers;j++){
				poolpipes[i].users[j-1] = poolpipes[i].users[j];			
			}
			poolpipes[i].nusers--;
			for(j=0; j < poolpipes[0].nclientes && poolpipes[0].listaClientes[j].pid != nova.pid ; j++);
			for(j++; j < poolpipes[0].nclientes;j++){
				poolpipes[i].users[j-1] = poolpipes[i].users[j];			
			}
			poolpipes[0].nclientes--;
			printf("\n----------\nCliente saiu:\n PID:%d\nPipe %d tem agora %d clientes.\n----------\n", nova.pid, i, poolpipes[i].nusers);
		}

	}
}
void * thread_serverPipe(void* p){
	signal(SIGUSR1, thread_exit);
	cliente novo;
	int i,ret, maior;
	int users_ativos = 0;
	thread * poolpipes = (thread *) p;
	resposta novoCliente;
	int npipes = poolpipes[0].npipes;
	
	mkfifo(poolpipes[0].pipename, 0666);
	int f = open(poolpipes[0].pipename, O_RDONLY);
	int f1 = open(poolpipes[0].pipename, O_WRONLY);
	if(f == -1){
		printf("Erro fifo\n");
		return NULL;
	}

	while(1){
		int ret;
		read(f,&novo, sizeof(cliente));
		ret = checkUser(poolpipes[0].database, novo.username);
		if(ret < 0){
			printf("O ficheiro %s nao existe!\n",poolpipes[0].database);
		}else if(ret == 0){
			novoCliente.tipo = 3;
			novoCliente.pid = novo.pid;
			tratar_resposta(&novoCliente, poolpipes);
		}else {
			int check = 0;
			for (i = 0; i < poolpipes[0].nclientes ; ++i) {
				if (strcmp(novo.username, poolpipes[0].listaClientes[i].username) == 0){
					check = 1;
				}
			}
			if (check == 0) {
				maior = poolpipes[1].nusers;
				for (i = 1; i < npipes; i++) {
					if (poolpipes[i].nusers < maior) {
						poolpipes[i].users[poolpipes[i].nusers] = novo.pid;
						poolpipes[i].nusers++;
						novoCliente.pipe = i;
						novoCliente.tipo = 2;
						novoCliente.pid = novo.pid;
						poolpipes[0].listaClientes[poolpipes[0].nclientes].pid = novo.pid;
						strcpy(poolpipes[0].listaClientes[poolpipes[0].nclientes].username, novo.username);
						poolpipes[0].listaClientes[poolpipes[0].nclientes].linha = 0;
						poolpipes[0].nclientes++;
						break;
					}
				}
				if (i == npipes) {
					poolpipes[1].users[poolpipes[1].nusers] = novo.pid;
					poolpipes[1].nusers++;
					i = 1;
					novoCliente.pipe = 1;
					novoCliente.tipo = 2;
					novoCliente.pid = novo.pid;
					poolpipes[0].listaClientes[poolpipes[0].nclientes].pid = novo.pid;
					strcpy(poolpipes[0].listaClientes[poolpipes[0].nclientes].username, novo.username);
					poolpipes[0].listaClientes[poolpipes[0].nclientes].linha = 0;
					poolpipes[0].nclientes++;
				}
				tratar_resposta(&novoCliente, poolpipes);
				printf("\n----------\nCliente ligado: %s\nPID:%d\nPipe atribuido: %d\n----------\n", novo.username,
					   novo.pid, i);
			}else{
				novoCliente.tipo = 4;
				novoCliente.pid = novo.pid;
				tratar_resposta(&novoCliente, poolpipes);
			}
		}
	}
}
int checkUser(char * filename, char * username){
	FILE *file;
	char line[20];
	char name[10];
	int check = 0;
	file = fopen(filename, "r");
	if (file){
//		while ((getline(&line, &len, file)) != -1) {
		while ((fscanf(file,"%s",line)) != EOF) {
			strcpy(name,line);
			strcat(name,"\n");
			if (strncmp(name,username,(strlen(username) -1)) == 0){
				check = 1;
			}
		}
		fclose(file);
		if (check == 1){
			return 1;
		}else{
			return 0;
		}
	}else{
		return -1;
	}
}

void estatisticas(thread * t){
	int i,j,k,l,charcount=0, wordcount=0;
	int chars[200][2], nchars=0, best[5][2], max=0;
	for(i=0; i < 200; i++){
		chars[i][0]='\0';
		chars[i][1]=0;
	}
	for(i=0; i<5;i++){
		best[i][0]='\0';
		best[i][1]=0;
	}

	for(i=0; i<t[0].nlinhas; i++) {
   		for(j=0; j<t[0].ncolunas; j++){
			if(t[0].text[i][j] != ' ' && t[0].text[i][j] != '\0'){
				charcount ++;
				if(nchars == 0){
					chars[0][0]=t[0].text[i][j];
					nchars++;
					chars[i][1]++;
				}else{
					for(k=0;k<nchars && chars[k][0] != t[0].text[i][j] ;k++);
					chars[k][0] = t[0].text[i][j];
					if(k == nchars)nchars++;
					chars[i][1]++;
				}
			}
		}
	}
	for(j=0; j<5; j++){
		for(i=0; i<nchars; i++){
			if(max < chars[i][1]){
				max = chars[i][1];
			}
		}
		for(i=0; i<nchars; i++){
			if(max == chars[i][1]){
				best[j][0] = chars[i][0];
				best[j][1] = chars[i][1];
				chars[i][1] = 0;
			}
		}
		max = 0;
	}

	for(i=0; i<t[0].nlinhas; i++) {
   		for(j=0; j<t[0].ncolunas; j++){
			if(t[0].text[i][j] != ' ' && t[0].text[i][j] != '\0'){
				wordcount++;			
			}
			for(; j<t[0].ncolunas && t[0].text[i][j] != ' '; j++);
		}
	}
	
	for(i=0;i < 5;i++)
		printf("%d Caracter: %c contagem: %d\n", i+1,best[i][0], best[i][1]);

	printf("Nº Letras: %d\n", charcount);
	printf("Nº Palavras: %d\n\n", wordcount);
	
}


