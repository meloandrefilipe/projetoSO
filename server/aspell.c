//
// Created by andre on 30/11/18.
//

#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdlib.h>
#include <errno.h>
#include <pthread.h>
#include <string.h>
#include <sys/wait.h>
#include "aspell.h"

#define size 2048


int checkSpell(char *str){
    char newString[100][100];
    int i, j=0, ctr=0, w = 0, errors = 0;
	for(i=0; str[i] == ' ';i++);
    for(;i<=(strlen(str));i++) {
        if (str[i] == ' ' || str[i] == '\0') {
            newString[ctr][j] = '\0';
		//for(; str[i] == ' ';i++);
            ctr++;
            j = 0;
        } else {
            newString[ctr][j] = str[i];
            j++;
        }
    }
    while (strcmp(newString[w], "\0") != 0){
        errors = errors + aspell(newString[w]);
        w++;
    }
    return errors;
}

int aspell(char *str){
    int pipe1[2], pipe2[2];
    pid_t pid;

    if (pipe(pipe1) == -1){
        perror("pipe");
        exit(EXIT_FAILURE);
    }
    if (pipe(pipe2) == -1){
        perror("pipe");
        exit(EXIT_FAILURE);
    }

    pid = fork();

    if(pid < 0){
        perror("fork");
        exit(EXIT_FAILURE);
    }


    if(pid > 0){ // PAI
        char buffer[size];
        char *buffer2;
        const char s[2] = "\n";
        char check = '*';

        close(pipe1[0]); // Fecha a leitura do pipe 1
        write(pipe1[1],str, sizeof(str)+1);
        close(pipe1[1]);

        wait(NULL);

        close(pipe2[1]); //fecha a escrita do pipe 2
        read(pipe2[0],buffer,size);
        strtok(buffer, s);
        buffer2 = strtok(NULL, s);

        //printf("%c\n", buffer2[0]);
        close(pipe2[0]);
        if(buffer2[0] == check){
            return 0;
        }else{
            return 1;
        }

    }else{ // FILHO
        dup2(pipe2[1], STDOUT_FILENO);
        dup2(pipe1[0], STDIN_FILENO);
        close(pipe1[1]); //fechar a escrita no primeiro pipe
        execlp("aspell","aspell","pipe",NULL);

        //fechar os dois lados de leitura
        close(pipe1[0]);
        close(pipe2[0]);

        close(pipe2[1]); //Fecha a parte de leitura do pipe 2
        exit(0);
    }
}
