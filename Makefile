build: servidor cliente

cliente: ./client/client.o
	cc -o ./client/cliente ./client/client.o -lncurses -pthread

client.o: ./client/client.c
	cc -c ./client/client.c

servidor: ./server/server.o
	cc -o ./server/servidor ./server/server.c ./server/aspell.c ./server/aspell.h -pthread

clear:
	rm -f ./client/client.o ./client/cliente ./server/server.o ./server/servidor
