#include <stdlib.h>
#include <string.h>
#include <ncurses.h>
#include <stdio.h>
#include <unistd.h>
#include <ctype.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <stdbool.h> 
#include <sys/time.h>
#include <sys/stat.h>
#include <pthread.h>
#include <fcntl.h>
#include "../server/medit.h"
#include "client.h"

int SelectLine(int xatual, int yatual,int max_columns,char** texto, WINDOW* win, int pipe_write);
void atualizaLinha(char** texto, int linha, WINDOW* win);
void * thread_leituraPipe(void * p);
void thread_exit();

int yglobal = 0;
int xglobal = 0;
int pipe_read_glob;

int main(int argc, char **argv, char **envv){
    if(getenv("MEDIT_MAXLINES")==NULL || getenv("MEDIT_MAXCOLUMNS")==NULL){
        printf("As variaveis de ambiente ainda nao foram criadas!\n");
        return-1;
    }
	int max_rows = atoi(getenv("MEDIT_MAXLINES"));
	int max_columns = atoi(getenv("MEDIT_MAXCOLUMNS"));
	cliente enviarinfo;
	
	char *linha;
	int i,j;
	char username[50], pipe_atribuido[7];
	char strPid[12];	
	int pipe_read, pipe_write;
	if(argc == 1 || argc == 3){
		if (argc == 3){
			if (strcmp(argv[1],"-u") != 0){
				printf("\nArgumento nao existe\n\n");
				return -1;
			}
			strcpy(username,argv[2]);
            strcat(username,"\n");
		}else{
			for(i=0;i<40;i++){printf("\n");}
			printf("Introduza o username:\n");
			fgets(username,9,stdin);
		}
  	}else{
  	 	printf("Verifique os parametros deste comando\n");
    		return 0;
  	}
	strcpy(enviarinfo.username,username);
	enviarinfo.pid = getpid();
	enviarinfo.linha = 0;

//----------ENVIAR INFO DO CLIENTE AO SERVIDOR
	printf("\nÀ espera do servidor...\n");
	int f = open("../server/serverPipe", O_WRONLY);
	write(f,&enviarinfo,sizeof(cliente));
	close(f);
//----------ABRIR PIPE PARA LEITURA DA RESPOSTA
	
	sprintf(strPid, "%d", getpid());
	mkfifo(strPid,0666);
	pipe_read = open(strPid, O_RDONLY);
	int pipe_read_w = open(strPid, O_WRONLY);
	resposta r_pipe;
	read(pipe_read, &r_pipe, sizeof(resposta));
	
	if(r_pipe.tipo == 4){
		printf("\nUtilizador já logado\n");
		return -1;
	}
	if(r_pipe.tipo == 3){
		printf("\nUtilizador não existe\n");
		return -1;
	}
//----------ABRIR PIPE PARA ENVIO DE INFORMAÇÃO CORRENTE
	char strPipe[12];
	char temp[50] = "../server/pipe";
	sprintf(strPipe,"%d",r_pipe.pipe);
	strcat(temp,strPipe);
	pipe_write = open(temp, O_WRONLY);

	//ALOCAR MEMORIA PARA O TEXTO
	char **texto = malloc(sizeof(char *) * max_rows);
	for (i = 0; i < max_rows; ++i) {
    		texto[i] = malloc(sizeof(char) * (max_columns+2));
	}
	//LER TEXTO DO PIPE
	resposta r;
	for(i=0; i < max_rows; i++){
		read(pipe_read, &r, sizeof(resposta));
		strncpy(texto[i],r.msg.linha, max_columns+2);
	}
	
	for(i=0;i<max_rows;i++){
		for(j=0;j<max_columns;j++){
			if (texto[i][j] == '\0'){
				texto[i][j] = ' ';
			}
		}
	}
	pipe_read_glob = pipe_read;
	initscr();
	noecho();
	cbreak();
	start_color();

/*	
*/
	
	
	int ymax, xmax, ret;
	int xactual,yactual;
	getmaxyx(stdscr, ymax, xmax);
	char oldchar, newchar;
	int ycordWin = (ymax/2)-(max_rows/2);
	int xcordWin = (xmax/2)-(max_columns/2);
	int key;
	WINDOW * win = newwin(max_rows + 4,max_columns + 4, ycordWin, xcordWin);
	refresh();
	wrefresh(win);
	init_pair(1, COLOR_GREEN, COLOR_GREEN);
	init_pair(2, COLOR_RED, COLOR_RED);

	pthread_t id_thread;
	threadd t;
	t.texto = texto;
	t.nlinhas = max_rows;
	t.ncolunas = max_columns;
	t.win = win;
	t.pipe_read = pipe_read;
	t.ycordwin= ycordWin;
	t.xcordwin= xcordWin;
	
	

	for(i=0;i<max_rows;i++){
		mvwprintw(win,2+i,2,"%s\n",texto[i]);
	}

	

	for(i=2; i<max_rows+2;i++){
		mvprintw(ycordWin+i,xcordWin-9, "Linha %d:", i-1);
		attron(COLOR_PAIR(1));
		mvprintw(ycordWin+i,xcordWin-11, "%c", ' ');
		attroff(COLOR_PAIR(1));
		wrefresh(stdscr);
	}
	t.yatual = 2;
	t.xatual = 2;
	ret = pthread_create(&id_thread,NULL,thread_leituraPipe,(void *) &t);
	if(ret != 0){
			printf("\n\nErro thread\n\n");
			return -1;
	}
	box(win,0,0);
	wrefresh(win);
	yglobal=2;
	xglobal=2;
	wmove(win,2,2);
	wrefresh(win);
	xactual = 2;
	yactual = 2;
	keypad(win, TRUE);
	keypad(stdscr, TRUE);

	linhas req;
	req.nlinha = yactual-1;
	req.pid = getpid();
	req.submit = 0;
	req.request = 0;
	resposta respostaRequest;
	while((key=getch()) != 27){ //ESCAPE	
		req.submit = 0;
		if(key == KEY_UP){
			yactual--;
			if(yactual < 2){yactual=2;}
			
		}
		if(key == KEY_DOWN){
			yactual++;
			if(yactual >= 2 + max_rows){yactual=1+max_rows;}
		}
		if(key == KEY_LEFT){
			xactual--;
			if(xactual < 2){xactual=2;}
			
		}
		if(key == KEY_RIGHT){
			xactual++;
			if(xactual > 2 + max_columns){xactual=2+max_columns;}
		}
		if(key == 10){
			req.request = 1;
			req.nlinha = yactual - 1;
			write(pipe_write,&req, sizeof(linhas));
			read(pipe_read,&respostaRequest, sizeof(resposta));
			if(respostaRequest.nlinha == 0){
				attron(COLOR_PAIR(2));
				mvprintw(yactual+ycordWin,xcordWin-11, "%c", ' ');
				attroff(COLOR_PAIR(2));
				wmove(win,yactual,xactual);	
				refresh();
				wrefresh(win);
				xactual=SelectLine(xactual, yactual,max_columns, texto, win, pipe_write);
			
				attron(COLOR_PAIR(1));
				mvprintw(yactual+ycordWin,xcordWin-11, "%c", ' ');
				attroff(COLOR_PAIR(1));
			}
			req.request = 0;
		}
		yglobal=yactual;
		xglobal=xactual;
		wmove(win,yactual,xactual);
		refresh();
		wrefresh(win);
	}
	for (i = 0; i < max_rows; i++) {
    		free(texto[i]);
	}
	endwin();

	linhas sair;
	sair.pid = getpid();
	sair.nlinha = -1;
	write(pipe_write,&sair,sizeof(linhas));

	free(texto);
	
	close(pipe_write);
	close(pipe_read_w);
	close(pipe_read);
	sprintf(strPid, "%d", getpid());
	remove(strPid);
	pthread_kill(id_thread, SIGUSR1);
	pthread_join(id_thread, NULL);
	return 0;
}

void atualizaLinha(char** texto, int linha, WINDOW* win){

	mvwprintw(win,2+linha,2,"%s\n",texto[linha]);
	box(win,0,0);
	wrefresh(win);

}

int SelectLine(int xactual, int yactual,int max_columns,char** texto,WINDOW* win, int pipe_write){
	int i,j=0;
	int key;
	char temp,temp2;
	FILE *f;
	linhas atualizar;
	atualizar.nlinha = yactual-1;
	atualizar.pid = getpid();
	atualizar.submit = 0;
	atualizar.request = 0;
	while(1){
	while((key=getch()) != 10){ //ENTER	
		if(key == KEY_LEFT){
			xactual--;
			if(xactual < 2){xactual=2;}
		}
		if(key == KEY_RIGHT){
			xactual++;
			if(xactual > 2 + max_columns){xactual=2+max_columns;}
		}
		if(key == KEY_BACKSPACE){ 
			if(xactual-2 != 0){
				for(i=xactual-2;texto[yactual-2][i]!='\0';i++){
					texto[yactual-2][i-1]=texto[yactual-2][i];
				}
				texto[yactual-2][i-1]=' ';
				//atualiza no ecrã
				atualizaLinha(texto,yactual-2, win);
				//envia para o servidor
				strcpy(atualizar.linha,texto[yactual-2]);
				atualizar.submit = 0;
				write(pipe_write,&atualizar,sizeof(linhas));
				xactual--;
				box(win,0,0);
			}		
		}
		if((key >= 'a' && key <= 'z') || (key >='A' && key <= 'Z') || (key >= 33 && key <= 63) || key == ' '){		
			if(texto[yactual-2][max_columns-1]== ' ' || texto[yactual-2][max_columns-1]== '\0'){				
				for(i=xactual-2;i<max_columns-1;i++){
					if(i==xactual-2){
						temp = texto[yactual-2][i+1];
						texto[yactual-2][i+1]=texto[yactual-2][i];
					
					}else{	
						temp2 = texto[yactual-2][i+1];
						texto[yactual-2][i+1]=temp;
						temp=temp2;
					}
				
				}
				xactual++;
				if(xactual > 2 + max_columns){xactual=2+max_columns;}
				texto[yactual-2][xactual-2-1]=key;
				//atualiza no ecrã
				atualizaLinha(texto,yactual-2, win);
				//envia para o servidor
				strcpy(atualizar.linha,texto[yactual-2]);
				atualizar.submit = 0;
				write(pipe_write,&atualizar,sizeof(linhas));
				box(win,0,0);
			}
		}
		xglobal=xactual;	
		wmove(win,yactual,xactual);
		wrefresh(win);
	}
	// aqui é quando é enviado a linha editada
	atualizar.submit = 1;
	resposta erros;
	strcpy(atualizar.linha,texto[yactual-2]);
    	write(pipe_write,&atualizar,sizeof(linhas));
	read(pipe_read_glob,&erros,sizeof(resposta));
	atualizar.submit = 0;
	if(erros.tipo == 7){
		
		return xactual;
	}
}
	
}
void * thread_leituraPipe(void * p){
	signal(SIGUSR1, thread_exit);
	threadd * t = (threadd *) p;
	resposta r;

	while(1){
		read(t->pipe_read,&r,sizeof(resposta));
		if(r.tipo == 0){
			strncpy(t->texto[(r.msg.nlinha)-1], r.msg.linha, t->ncolunas+2);
			atualizaLinha(t->texto, r.msg.nlinha - 1, t->win);
			wmove(t->win,yglobal,xglobal);
			wrefresh(t->win);
		}
		if(r.tipo == -1){
			char strPid[12];
			endwin();
			printf("\nO servidor fechou\n");
			sprintf(strPid, "%d", getpid());
			remove(strPid);	
			free(t->texto);					
			exit(0);
		}
		if(r.tipo == 1){
			//printf("dud\n");
		}
		if(r.tipo == 5){
				attron(COLOR_PAIR(2));
				mvprintw(r.nlinha+1+t->ycordwin,t->xcordwin-11, "%c", ' ');
				attroff(COLOR_PAIR(2));
				wmove(t->win,yglobal,xglobal);
				refresh();
				wrefresh(t->win);
		}
		if(r.tipo == 6){
				attron(COLOR_PAIR(1));
				mvprintw(r.nlinha+1+t->ycordwin,t->xcordwin-11, "%c", ' ');
				attroff(COLOR_PAIR(1));
				wmove(t->win,yglobal,xglobal);
				refresh();
				wrefresh(t->win);

		}
	}
}
void thread_exit(){
	pthread_exit(NULL);
}
